/// DEFAULTS --- DEFAULTS --- DEFAULTS ///
defaults =
{ // Defaults
  p :
  { // Player Defaults
    healthpoints : 100,
    defence : 1,
    speed : 1,
    agility : 1,
    sleepiness : 0,
    weildiness : 0,
  },
  ethereal :
  { // Ethereal Defaults
    max_hp : 1,
  },
  physical :
  {
    weight : 1,
  },
  input_device :
  {
      
  },
}

/// ENUMS --- ENUMS --- ENUMS ///
enum Start_mode
{
  NEW_GAME,
  SELECTED_SAVE,
  LAST_SAVE
}
enum Controller_types
{
  NONE,
  KEYBOARD_MOUSE,
  KEYBOARD_ONLY,
  GAMEPAD_GENERIC,
  GAMEPAD_PS4,
  GAMEPAD_XBX360,
  GAMEPAD_XBXONE,
}

/// Item codes ///
