// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Player() : Humanoid() constructor
{
  static setup = function()
  {
    hp = global.defaults.p.healthpoints;
    def = global.defaults.p.defence;
    spd = global.defaults.p.speed;
    agl = global.defaults.p.agility;
    sleepiness = global.defaults.p.sleepiness;
    wieldiness = global.defaults.p.wieldiness;
  }
  
  // controller = ;
  // predicted_next_room = ;
}