
function Physical() : Ethereal() constructor
{
  // A Physical is a game entity that exists in the
  // 
  coord =
  {
    X : 0,
    Y : 0,
    Z : 0
  }
  size =
  {
    X : 0,
    Y : 0,
    Z : 0
  }
  interactivity =
  {
    // Handling by 
    weildiness  : 0,
    weight      : 0,
    walk_over_pickup : false,
    pickup_possible  : true,
  }
}