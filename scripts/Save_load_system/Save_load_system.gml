// Handles, manages, and masters over files, directories, etc
function Save_load_system() constructor
{
  prefix    = "savegame_";
  filetype  = ".txt";
  folder    = "save";
  savedir   = working_directory + folder + "\\";
  def = 
  { // Defaults
    filename : prefix + string(date_current_datetime()) + filetype,
  }
  sf =
  { // Savefile-related
    current : def.filename,
    last    : prefix + "last" + filetype,
    last_exists : false,
  }
  load_last_filename = function()
  {
    show_message(savedir);
    if(file_exists(savedir + sf.last))
    {
      sf.last_exists = true;
    }
    else // else make file exist
    {
      var file = file_text_open_write(savedir + sf.last);
      file_text_close(file);
      sf.last_exists = true;
    }
  }
  setup = function()
  {
    if (directory_exists(savedir))
    {
      load_last_filename();
    } else {
      directory_create(savedir);
      load_last_filename();
    }
  }
  save = function()
  {
    var save_successful = false;
    var fname = get_save_filename_ext(filetype, def.filename, savedir, "SAVE GAME");
    if !(file_exists(fname))                          // If file doesn't exist
    {
      var file = file_text_open_write(fname);         // Open file
      var json = json_encode(global.game.game_data);  // Encode to JSON
      file_text_write_string(file, json);             // Write game_data contents
      file_text_close(file);                          // Close file
      save_successful = true;                         // Success
    }
    else
    {
      show_question(global.game.game_data[? "confirm_save"]);
    }
    return save_successful;
  }
  load = function(prompt_player)
  {
    var fname = "";
    if(prompt_player != true)
    {
      fname = get_open_filename_ext(filetype, def.filename, savedir, "LOAD GAME - Choose a save file!");
      
    } else {
      fname = sf.last;
    }
    var string_data = file_text_open_read(fname);
    var loaded_game_data = json_decode(string_data);
    return loaded_game_data;
  }
}