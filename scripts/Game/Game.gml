function Game() constructor
{
  // Modules(? idk) //
  sl = new Save_load_system();
  sl.setup();
  p = new Player();
  con = new Input_device();
  // Variables //
  initialized = false;
  game_data = ds_map_create();
  //ds_map_create();
  load_last_at_start = false; // todo: add to settings map?
  
  static init_game_data = function()
  {
    // Check if game should load last save immediately, according to settings
    if (!load_last_at_start)
    {
      // DEFAULT GAME DATA
      var last_save = sl.sf.last;
      ds_map_add(game_data, "frame_count", 0);
      ds_map_add(game_data, "player_count", 1);
      ds_map_add(game_data, "player", p);
      ds_map_add(game_data, "start_mode", Start_mode.NEW_GAME);
      ds_map_add(game_data, "txt_room_center", "TESTIING");
      ds_map_add(game_data, "pre_mm_wait", 100);
      ds_map_add(game_data, "confirm_save", "Do you wish to overwrite this save file?");
      //ds_map_add(game_data, "savedir", (working_directory + sl.folder + "\\"));
      return true;
    }
    else
    {
      sl.load(sl.sf.last);
      return true;
    }
  }
  
  run = function()
  {
    init_game_data();
    /// Handle game start - new vs. loaded
    var continue_running = false;
    if (game_data[? "start_mode"] == Start_mode.NEW_GAME)
    {
      // "New Game" - Start new game
      global.l.log(working_directory);
      continue_running = true;
    }
    else if ( game_data[? "start_mode"] == Start_mode.LAST_SAVE)
    {
      // "Continue" - Load game based 
      continue_running = sl.load(sl.sf.last);
    }
    else if ( game_data[? "start_mode"] == Start_mode.SELECTED_SAVE)
    {
      // "Load" - Load game based on player's chosen save
      //1: Ask player to choose filename to load.
      var default_savefile = sl.sf.last;
      var savefile_to_load = default_savefile;
      continue_running = sl.load(savefile_to_load);
    }
    /// Main run section /// MAIN - RUN - MAIN - RUN - MAIN - RUN ///
    
    /// End run items? TODO
  }
  pause = function()
  {
    //TODO
  }
  wait = function(time_to_wait)
  {
    var t = current_time + time_to_wait;
    while (current_time < t) { /* idle loop */ }
  }
  get = { // GETTERS
    player : function()
    {
      return game_data[? "player"];
    },
  }
  set = { // SETTERS
    player : function(plyr)
    {
      ds_map_replace(game_data, "player", plyr);
    },
    controller : function()
    {
      
    },
  }
  initialized = init_game_data();
}

l = new Logger();
l.debug_messages = true;
global.game = new Game();
global.game.run();