room_timer++;

fc = global.game.game_data[? "frame_count"];
d_text = global.game.game_data[? "frame_count"];
ds_map_replace(global.game.game_data, "frame_count", fc+1);

var wait_time = global.game.game_data[? "pre_mm_wait"];
var tmp_room_time = room_timer;
switch(room)
{
  case rm_MainMenu_PARENT:
    room_goto(rm_MainMenu_pre_0);
    break;
  case rm_MainMenu_pre_0:
    if(tmp_room_time > wait_time) {
      //global.game.wait(wait_time);
      room_goto(rm_MainMenu_splash);
    }
    break;
  case rm_MainMenu_splash:
    if(tmp_room_time > wait_time) {
      room_goto(rm_MainMenu_main);
    }
    break;
  case rm_MainMenu_main:
    // Player will choose
    break;
}
tmp_room_time = 0;